import { Component, OnInit } from '@angular/core';
import { PostService } from '../service/post.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  result : any = '';
  constructor(private postService : PostService) { }

  ngOnInit(): void {
    this.postService.lista().subscribe(
      data => {
        console.log("Lista done")
        this.result = data;
      },
      err => {
        console.log("error")
      }
    )
  }

}
