import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from '../service/post.service';
@Component({
  selector: 'app-postdetail',
  templateUrl: './postdetail.component.html',
  styleUrls: ['./postdetail.component.css']
})
export class PostdetailComponent implements OnInit {
  id: String = '';
  result: any = '';
  receivedDate: string = "";
  receiveEvent(event: string){
      this.receivedDate = event;
  }
  constructor(private postService: PostService,
    private route: ActivatedRoute) { }

    
  ngOnInit(): void {
    this.id = JSON.parse(this.route.snapshot.paramMap.get('id') || '{}');
    this.postService.detail(this.route.snapshot.params.id).subscribe(
      data => {
        this.result = data;
      }
    );
  }
}
