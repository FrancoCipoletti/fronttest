import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
 

@Injectable({
  providedIn: 'root'
})
export class PostService {
  url: String = "https://jsonplaceholder.typicode.com/";
  constructor(private httpClient : HttpClient) { }

  public lista(): Observable<any> {
    return this.httpClient.get<any[]>(this.url + 'posts');
  }

  public detail(id: number): Observable<any> {
    return this.httpClient.get<any>(this.url + `posts/${id}`);
  }


 // public coments(id: number): Observable<any> {
    //return this.httpClient.get<any>(this.url + `posts/${id}`); ??
  //}
}
