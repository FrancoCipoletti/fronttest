import { Component,Output,EventEmitter, Input, OnInit } from '@angular/core';
import { PostService } from '../service/post.service';

@Component({
  selector: 'app-comentarios',
  templateUrl: './comentarios.component.html',
  styleUrls: ['./comentarios.component.css']
})
export class ComentariosComponent implements OnInit {
  nombre: string = '';
  mail: string = '';
  descripcion: string = '';
  index: any;
  @Input('idpost') public idPost!: String;
  comentarios:{ nombre: string;mail: string; descripcion: string; }[] = [{ nombre: "Franco", mail: "cipoletti.f@gmail.com" , descripcion: "Esto es una descripcion hardcodeada" }];
  constructor( private postService: PostService) {  }
   @Output()
   buttonClicked: EventEmitter<string> = new EventEmitter<string>();
   sendEvent(){
     this.buttonClicked.emit("2022/02/31")
   }

    validateEmail (email : string) {
    return String(email)
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
  };
   newComentario(){

        if (this.nombre.length>0 && this.nombre.length<255){
          if (this.validateEmail(this.mail)) {
            if (this.descripcion.length>0 && this.descripcion.length<500){
    this.comentarios.push({ nombre: this.nombre, mail:this.mail, descripcion: this.descripcion });
     this.nombre='';
     this.descripcion='';
     this.mail=''
    
    }
    else {
     window.alert("La descripcion debe contener al menos 1 caracter y menos de 500.");
     }
    } else {
      window.alert("Mail no valido");
    }}
    else {
     window.alert("El nombre debe contener al menos 1 caracter y menos de 255.");}

   }

   deleteComentario(index: number){
     console.log('delete'+index);
   this.comentarios.splice(index,1);
   }
  ngOnInit(): void {

    /*  this.postService.coments(+(this.idPost)).subscribe(
      data => {
        this.comentarios = data;
      }
    ); 
    No encontre los comentarios o su url, asi que inicialice la lista manualmente.
    */ 
  } 

}
