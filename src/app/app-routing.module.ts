import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotfoundComponent } from './notfound/notfound.component';
import { PostComponent } from './post/post.component';
import { PostdetailComponent } from './postdetail/postdetail.component';

const routes: Routes = [
  {path:'posts', component:PostComponent },
  {path:'posts/:id', component:PostdetailComponent},
  {path:'**', redirectTo:'/404'},
  {path:'404', component:NotfoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

